<!DOCTYPE t:stylesheet [
  <!ENTITY s "http://suika.fam.cx/admin/assign/2005/urnreg#">
]>
<t:stylesheet version="1.0"
        xmlns:t="http://www.w3.org/1999/XSL/Transform"
        xmlns="http://www.w3.org/1999/xhtml"
        xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
        xmlns:prop="http://suika.fam.cx/admin/assign/urn-20050519#DT-"
        xmlns:map="http://suika.fam.cx/~wakaba/2005/6/uri-table#"
        xmlns:replace="http://suika.fam.cx/~wakaba/archive/2005/6/replace#"
        xmlns:s="http://suika.fam.cx/admin/assign/2005/urnreg#"
        xmlns:dc="http://purl.org/dc/elements/1.1/"
        xmlns:foaf="http://xmlns.com/foaf/0.1/">

  <t:import href="/www/2005/urimap/urimap-html.ja.xsl"/>

  <t:template name="document-class">status-work-in-progress</t:template>

  <t:template name="document-title">
    $BEPO?:Q$_(B &lt;urn:x-suika-fam-cx:&gt; sub-nid  $B$N0lMw(B
  </t:template>

  <t:template name="document-header" xml:space="preserve">
    <h1>$BEPO?:Q$_(B <code class="URI">&lt;urn:x-suika-fam-cx:&gt;</code>
        <code class="ABNF" lang="en" xml:lang="en">sub-nid</code>  $B$N0lMw(B</h1>
  </t:template>

  <t:template name="document-footer">
    <div class="footer" xml:space="preserve">
      [<a href="/" rel="home">/</a> <a href="/search/" rel="search">$B8!:w(B</a>]
      [<a href="/admin/">$B4IM}It(B</a> <a href="/admin/assign/">$BEPO?L>78(B</a>
       <a href="/admin/assign/urn"><code class="URI"
       >&lt;urn:x-suika-fam-cx:&gt;</code></a>
       <a>$BEPO?:Q$_0lMw(B</a>]
    </div>
  </t:template>

  <t:template name="document-style-sheet-uri"
  >/admin/style/common</t:template>

  <t:template name="document-style-sheet-add-uri"
  >/www/style/def/uri</t:template>

  <t:template name="entry-props">
    <t:param name="entry"/>
    <t:param name="entry-type"/>

    <t:for-each select="$entry/child::s:currentRegistration |
                        $entry/child::s:registration">
      <t:apply-templates select="/child::rdf:RDF/child::s:Registration
                               [string(attribute::rdf:about) =
                                string(current()/attribute::rdf:resource)]"
        mode="template"/>
    </t:for-each>

    <div class="section">
      <h3>$B2r7h(B</h3>
      <dl>
        <t:call-template name="entry-map-props">
          <t:with-param name="entry" select="self::node()"/>
          <t:with-param name="entry-type" select="string($entry-type)"/>
        </t:call-template>
      </dl>
    </div>

    <t:if test="$entry/child::*[not(self::prop:*) and not (self::s:*) and
                                not(self::map:*)]">
      <dl>
        <t:apply-templates
          select="$entry/child::*[not(self::prop:*) and not (self::s:*) and
                                  not(self::map:*)]"/>
      </dl>
    </t:if>
  </t:template>

  <t:template match="child::s:Registration"/>

  <t:template match="child::s:Registration" mode="template">
    <div class="section">
      <h3>$BEPO?(B ($BBh(B<t:value-of select="child::s:versionNumber"/>$BHG(B)</h3>
      <dl class="urn-suika-sub-nid-def">
        <t:apply-templates select="child::*"/>
      </dl>
    </div>
  </t:template>

  <t:template match="child::s:subNID">
    <dt><code class="ABNF" lang="en" xml:lang="en">sub-nid</code></dt>
    <dd><code class="URI"><t:apply-templates/></code></dd>
  </t:template>

  <t:template match="child::s:versionNumber">
    <dt>$BHGHV9f(B</dt>
    <dd><code><t:apply-templates/></code></dd>
  </t:template>

  <t:template match="child::s:registeredDate">
    <dt>$BEPO?F|(B</dt>
    <dd><code><t:apply-templates/></code></dd>
  </t:template>

  <t:template match="child::s:registrant">
    <dt>$BEPO?<T(B</dt>
    <dd><t:apply-templates select="/child::rdf:RDF/child::*
                                   [string(attribute::rdf:about) =
                                    string(current()/attribute::rdf:resource)]"
                           mode="agent"/></dd>
  </t:template>

  <t:template match="child::s:status">
    <dt>$B>uBV(B</dt>
    <dd>
      <t:choose>
      <t:when test="string(attribute::rdf:resource) = '&s;Status.Deprecated'">
        $BHs?d>)(B
      </t:when>
      <t:when test="string(attribute::rdf:resource) = '&s;Status.Historic'">
        $BNr;KE*(B
      </t:when>
      <t:otherwise>
        <t:call-template name="uriref">
          <t:with-param name="uri" select="string(attribute::rdf:resource)"/>
        </t:call-template>
      </t:otherwise>
      </t:choose>
    </dd>
  </t:template>

  <t:template match="child::s:externalSpecification">
    <dt>$B30It;EMM=q(B</dt>
    <dd>
      <t:call-template name="uriref">
        <t:with-param name="uri" select="string(attribute::rdf:resource)"/>
      </t:call-template>
    </dd>
  </t:template>

  <t:template match="child::dc:description">
    <dt>$B@bL@(B</dt>
    <dd>
      <t:attribute name="xml:lang">
        <t:value-of select="attribute::xml:lang"/>
      </t:attribute>
      <t:apply-templates/>
    </dd>
  </t:template>

  <t:template match="/child::rdf:*/child::foaf:*"/>

  <t:template match="/child::rdf:RDF/child::*" mode="agent">
    <dl>
      <t:apply-templates/>
    </dl>
  </t:template>

  <t:template match="child::foaf:name">
    <dt>$BL>A0(B</dt>
    <dd>
      <t:attribute name="xml:lang">
        <t:value-of select="attribute::xml:lang"/>
      </t:attribute>
      <t:apply-templates/>
    </dd>
  </t:template>

  <t:template match="child::foaf:mbox">
    <dt>$BEE;R%a%$%k(B</dt>
    <dd>
      <t:choose>
      <t:when test="starts-with(attribute::rdf:resource, 'mailto:')">
        <t:call-template name="mailref">
          <t:with-param name="mail"
               select="substring(attribute::rdf:resource, 8)"/>
        </t:call-template>
      </t:when>
      <t:otherwise>
        <t:call-template name="uriref">
          <t:with-param name="uri" select="string(attribute::rdf:resource)"/>
        </t:call-template>
      </t:otherwise>
      </t:choose>
    </dd>
  </t:template>

  <t:template name="text-uri-group"/>
</t:stylesheet>
