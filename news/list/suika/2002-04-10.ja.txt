suika.* $B%K%e!<%9AH0lMw(B
~~~~~~~~~~~~~~~~~~~~~~

    $B$3$NI=$O!"(B suika $B$G;O$^$k%K%e!<%9AH$r0lMw$K$7$?$b$N$G!"(B
  $B5$$,8~$$$?;~$K(B suika.announce,suika.robamimi,suika.misc
  $B$KEj9F$5$l$^$9!#(B
  
    $B$3$NI=$N!V(B+$B!W$G;O$^$k9T$r<h$j=P$7!"9TF,$N!V(B+$B!W$r(B
  $B<h$j=|$/$3$H$G!"(B checkgroups $BMQ%G!<%?$rMQ0U=PMh$^$9!#(B
  
  $B:G?7HG(B
  	<http://suika.fam.cx/admin/news/newsgroups-suika>
  $B$3$NHG(B (2002$BG/(B4$B7n(B10$BF|(B)
  	<urn:x-suika-fam-cx:admin:21>
  $BA0$NHG(B (2001$BG/(B12$B7n(B10$BF|(B)
  	<mid:1007978352.1592333.send.pl@wakaba.suika.fam.cx>

$B!{(Bsuika.*	$B@>1;7W2h$N4IM}$9$k%K%e!<%9AH72(B

+suika.admin	Administration of Suika Project/Server
	$B@>1;7W2h!&%5!<%P!<$N4IM}$K$D$$$F!#(B

+suika.announce	Announcement of Suika Project/Server, news groups, and so on.
	$B@>1;7W2h!&%5!<%P!<(B, $B%K%e!<%9!&%0%k!<%W(B, $B$=$NB>$N$*CN$i$;!#(B
	suika.* $B$NMxMQ<T$OI,$:9XFI$9$k$3$H!#(B
	Suika-Announce list $B$KEj9F$5$l$?5-;v$O<+F0E*$K$3$N%K%e!<%9AH$K$b(B
	$BEj9F$5$l$^$9!#(B

$B!&(Bsuika.chat $B$O(B2002$BG/(B4$B7n$KGQ;_$5$l$^$7$?!#(B

+suika.chuubu	Talks about chuubu, the Toyama Chuubu High School.
	$B$A$e!<$V(B ($BIY;3CfIt9b9;(B) $B$K$D$$$F$NOCBj!#(B

+suika.chuubu.r.55	Chuubu-Risuuka 55.
	$B$A$e!<$V$NBh(B55$B4|M}?t2J$K$D$$$F!#(B

$B!&(Bsuika.joke $B$O(B2002$BG/(B4$B7n$KGQ;_$5$l$^$7$?!#(B

+suika.markup	Markup languages and relatives (ex. stylesheet)
	$B%^!<%/IU$18@8l$d!"$=$l$K4X78$9$k$b$N(B ($B%9%?%$%k;f(B)$B$H$+$K$D$$$F(B

+suika.msg	Messaging system and network communication
	$B%a%C%;!<%87OE}$H%M%C%H%o!<%/$K$h$k0U;WABDL$K$D$$$F(B

+suika.okuchuu	Discussion/talk of Okuda Junior High School
	$B$*$/$A$e!#$K$D$$$F!#(B

$B!&(Bsuika.ren-ai $B$O(B2002$BG/(B4$B7n$KGQ;_$5$l$^$7$?!#(B

+suika.robamimi	Susumu no mimi ha roba no mimi!
	$B6+$S(B, $BFH$j8@(B, $B9pGr(B, $B$=$NB>!#FM$C9~$_$J$I$OB>$NE,@Z$J(B
	$B%0%k!<%W$K?6$j$^$7$g$&(B:-) 
	($B5l(B robamimi $B%a%$%k!&%j%9%H$OGQ;_$5$l$^$7$?!#(B)
	$B5-;v$O@>1;%5!<%P!<$G$O(B300$BF|4VJ]B8$7$^$9!#(B

+suika.software	Computer software and its technologies
	$B7W;;5!%=%U%H%&%'%"$H$=$N5;=Q$K$D$$$F(B

+suika.test	Very boring:-) test.
	$BEj9F$N3NG'$J$I!#3XNO;n83$K$D$$$F$N%0%k!<%W$G$O$J$$(B:-)
	Suika-Testers ML $B$KEj9F$5$l$?5-;v$O<+F0E*$K$3$N%K%e!<%9AH$K$b(B
	$BEj9F$5$l$^$9!#5-;v$O@>1;%5!<%P!<$G$O(B1$BF|4VJ]B8$7$^$9!#(B

+suika.tomikou	Discussion/talk/chat of Toyama High School
	$B$H$_$3$&$K$D$$$F!#(B

+suika.$B<B83(B	Experimental newsgroup with Han name.
	$B4A;zL>$N<B83E*?7J9AH!#(B

+suika.misc	Misc. talk, discussion.
	$B$=$NB>$NOCBj!#(B
	$B5-;v$O@>1;%5!<%P!<$G$O(B300$BF|4VJ]B8$7$^$9!#(B

$B!{(Bsuika.ml.*	$B%a%$%k!&%j%9%HO"F0%K%e!<%9AH(B
	$B$3$N3,AX$K$O!"%a%$%k!&%j%9%H$N5-;v$,<+F0E*$KEj9F$5$l$^$9!#(B
	$BDL>o!"5-;v$rEj9F$9$k$K$O3F%0%k!<%W$G$O$J$/!"Ej9FMQ$N%"%I%l%9$K(B
	$BEj$2$F2<$5$$!#(B

+suika.ml.admin	Administration ML of Suika Project.	(Moderated)
	$B@>1;7W2h$N4IM}(B ML ($BFI$`$N$K$O5v2D$,I,MW(B) ($BD>@\Ej9F=PMh$J$$(B)
	$B@>1;7W2h$d%5!<%P!<$K$D$$$F$ODL>o(B suika.admin $B$rMxMQ$7$^$7$g$&!#(B

+suika.ml.chuubu.brass	chubu-WindOrchestra ML	(Moderated)
	$BCfIt9b9;?aAU3ZIt$NO"MmMQ%a%$%k!&%j%9%H!#(B ($B1\Mw$K$O5v2D$,I,MW(B)
	($BD>@\Ej9F=PMh$J$$(B)

+suika.ml.himajin	Himajin ML	(Moderated)
	$B2K?M(B ML$B!#(B ($B1\Mw$K$O5v2D$,I,MW(B) ($BD>@\Ej9F=PMh$J$$(B)

($B0lMw$O$3$3$^$G(B)

    $B3F%K%e!<%9AH$K4XO"$9$k%a%$%k!&%j%9%H$K$D$$$F$O!"@>1;%5!<%P!<(B
  $B$G1?MQ$5$l$F$$$k%a%$%k!&%j%9%H$N0lMw(B <http://suika.fam.cx/ml/>
  $B$r8fMw2<$5$$!#(B
  
    suika.* $B%K%e!<%9AH$N2~JT(B ($B?7@_!&0\E>!&GQ;_$J$I(B) $B$K$D$$$F$O!"(B
  suika.admin $B$KDs0F$N5-;v$rEj9F$7$F2<$5$$!#(B
