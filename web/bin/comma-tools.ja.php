<?php 
  $req_uri = $HTTP_GET_VARS["uri"];
  $uri = "http://" . ($HTTP_HOST ? $HTTP_HOST : 'suika.fam.cx') . $req_uri;
  $encoded_uri = urlencode ($uri);

  $ereq_uri = htmlspecialchars ($req_uri);
  $euri = htmlspecialchars ($uri);
  $eencoded_uri = htmlspecialchars ($encoded_uri);
?>
<html xmlns="http://www.w3.org/1999/xhtml"
    lang="ja" xml:lang="ja">
<head profile="http://suika.fam.cx/~wakaba/-temp/wiki/wiki?WikiHTMLMetaProfile">
<title>&lt;<?php echo $euri?>&gt; $B$K$D$$$F(B</title>
<link rev="made" href="mailto:webmaster@suika.fam.cx" />
<link rel="copyright" href="/c/gnu/fdl" />
<link rel="stylesheet" href="/www/style/html/xhtml" media="all" />
</head>
<body>
<h1><code class="uri">&lt;<?php echo $euri?>&gt;</code> $B$K$D$$$F(B</h1>

<div class="section" id="INFO">
<h2>$B$3$NJG$K$D$$$F$N>pJs(B</h2>

<ul>
<li><a href="<?php echo $ereq_uri?>,headers">$B;*$,Aw$k(B 
<abbr lang="en" xml:lang="en" title="Hypertext Transfer Protocol">HTTP</abbr>
$BF,(B</a> <span class="weak">(<a href="http://cgi.w3.org/cgi-bin/headers" lang="en" xml:lang="en"><abbr
lang="en" xml:lang="en" title="World Wide Web Consortium">W3C</abbr>
<abbr lang="en" xml:lang="en" title="Hypertext Transfer Protocol">HTTP</abbr>
Head</a> $B$r;HMQ(B)</span></li>
<li><a href="http://web-sniffer.net/?verbose=on&sp=1&url=<?php echo $eencoded_uri?>">$B;*$,Aw$k(B 
<abbr lang="en" xml:lang="en" title="Hypertext Transfer Protocol">HTTP</abbr>
$BF,(B</a> <span class="weak">(<a href="http://web-sniffer.net/" lang="en" xml:lang="en">Web$B!>(BSniffer</a>
$B$r;HMQ(B)</span></li>
<li><a href="http://www.ircache.net/cgi-bin/cacheability.py?descend=on&amp;query=<?php echo $eencoded_uri?>">$B%-%c%C%7%e2DG=@-(B</a>
<span class="weak">(<a lang="en" xml:lang="en" 
href="http://www.mnot.net/cacheability/">Cacheability Engine</a> $B$r;HMQ(B)</span></li>
<li><a href="<?php echo $ereq_uri?>,tools"><code class="uri">&lt;<?php echo $euri?>&gt;</code>
$B$K$D$$$F(B</a></li>
</ul>
</div>

<div id="ALT" class="section">
<h2>$BB>$NHG(B</h2>

<ul>
<li><a href="<?php echo $ereq_uri?>,text">$BJ?J8HG(B</a>
<span class="weak">(<a href="http://cgi.w3.org/cgi-bin/html2txt" lang="en" xml:lang="en"><abbr
lang="en" xml:lang="en" title="World Wide Web Consortium">W3C</abbr> html2txt</a>
$B$r;HMQ(B)</span></li>
<li><a href="<?php echo $ereq_uri?>,cvslog">$BHG4IM}>pJs(B</a></li>
</ul>
</div>

<div class="section" id="VALIDATE">
<h2>$B8!>Z(B</h2>

<ul>
<li><a href="<?php echo $ereq_uri?>,validate"><abbr
lang="en" xml:lang="en" title="Hypertext Markup Language">HTML</abbr> / 
<abbr lang="en" xml:lang="en" title="Standard Generalized Markup Language"
>SGML</abbr>
$BBEEv@-8!>Z(B</a>
<span class="weak">(<a href="http://validator.w3.org/" lang="en" xml:lang="en"><abbr
lang="en" xml:lang="en" title="World Wide Web Consortium">W3C</abbr> <abbr
lang="en" xml:lang="en" title="Hypertext Markup Language">HTML</abbr> 
Validator</a> $B$r;HMQ(B)</span></li>
<li><a href="<?php echo $ereq_uri?>,lint"><abbr
lang="en" xml:lang="en" title="Hypertext Markup Language">HTML</abbr>
$B8!>Z(B</a>
<span class="weak">(<a href="http://openlab.ring.gr.jp/k16/htmllint/" lang="en" xml:lang="en"
>Another <abbr lang="en" xml:lang="en" title="Hypertext Markup Language"
>HTML</abbr>$B!>(BLint</a> $B$r;HMQ(B)</span></li>
<li><a href="<?php echo $ereq_uri?>,cssvalidate"><abbr
lang="en" xml:lang="en" title="Cascading Style Sheets">CSS</abbr> $B8!>Z(B</a>
<span class="weak">(<a href="http://jigsaw.w3.org/css-validator/" lang="en" xml:lang="en"><abbr
title="World Wide Web Consortium">W3C</abbr> <abbr
lang="en" xml:lang="en" title="Cascading Style Sheets">CSS</abbr>
Validator</a> $B$r;HMQ(B)</span></li>
<li><a href="<?php echo $ereq_uri?>,spell">$BDV$j$r8!>Z(B</a>
<span class="weak">(<a href="http://www.w3.org/2002/01/spellchecker" lang="en" xml:lang="en"><abbr
title="World Wide Web Consortium">W3C</abbr> Spellchecker</a> $B$r;HMQ(B)</span></li>
<li><a href="<?php echo $ereq_uri?>,checklink">$B2u$l$?%j%s%/$,$J$$$+8!>Z(B</a>
<span class="weak">(<a href="http://validator.w3.org/checklink" lang="en" xml:lang="en"><abbr
title="World Wide Web Consortium">W3C</abbr> Link Checker</a> $B$r;HMQ(B)</span></li>
<li><a href="<?php echo $ereq_uri?>,rchecklink">$B$3$NJG$H$3$NJG$+$i%j%s%/$7$F$$$kJG$K2u$l$?%j%s%/$,$J$$$+8!>Z(B</a>
<span class="weak">(<a href="http://validator.w3.org/checklink" lang="en" xml:lang="en"><abbr
title="World Wide Web Consortium">W3C</abbr> Link Checker</a> $B$r;HMQ(B)</span></li>
</ul>
</div>

<div class="section" id="NEARBY">
<h2>$B4XO"$9$k;q8;(B</h2>

<ul>
<li><a href="<?php echo $ereq_uri?>,imglist">$B$3$N3,AX$N2hA|0lMw(B</a>
(<a href="<?php echo $euri?>,imglist-detail">$B>\:Y>pJsIU$-(B</a>)</li>
<li><a href="./,tools">$B$3$N3,AX$K$D$$$F(B</a></li>
<li><a href="../,tools">$B0l$D>e$N3,AX$K$D$$$F(B</a></li>
</ul>
</div>

<div class="section" id="MISC">
<h2>$B$=$NB>(B</h2>

<ul>
</ul>
</body>
</html>
